import unittest

import pytest
import bitrise_trigger


class Test(unittest.TestCase):

    def test_args_1(self):
        args = bitrise_trigger.parse_args('-a a-tok -b b-tok -r ref -e foo-1=bar2 -e foo2=bar3 app'.split())
        assert args.api_token == 'a-tok'
        assert args.build_token == 'b-tok'
        assert args.ref == 'ref'
        assert args.env == ['foo-1=bar2', 'foo2=bar3']
        assert args.app_id == 'app'
        assert args.workflow == 'primary'

    def test_args_2(self):
        with pytest.raises(SystemExit):
            bitrise_trigger.parse_args('-b tok -r ref -e foo1=bar2 foo2=bar3 dangling'.split())

    def test_parse_env(self):
        envs = bitrise_trigger.parse_env(['foo-1=bar2', 'foo2=bar3'])
        assert envs == [
            {"mapped_to": "foo-1", "value": "bar2", "is_expand": True},
            {"mapped_to": "foo2", "value": "bar3", "is_expand": True}
        ]
